#------ Params --------#
T := 8 #cache size in KB
A := 8 #associativity
L := 64	#block size in B
TRACE := mcf.trace.gz

#------- Paths --------#
SRCDIR := ./src
INCDIR := ./include
OBJDIR := ./simulation
TARGET := cache

#------- Files --------#
SRCFILES 		:= $(wildcard $(SRCDIR)/*.cpp)

#------- Flags --------#
CC = g++
CFLAGS =-Wall -I $(INCDIR)


#------- Rules --------#
all: clean $(OBJDIR)/$(TARGET)

$(OBJDIR)/$(TARGET): $(SRCFILES)
	$(CC) $(CFLAGS) -o $(OBJDIR)/$(TARGET) $(SRCFILES)

clean:
	rm -f $(OBJDIR)/$(TARGET)

run: L2 VC

L2: $(OBJDIR)/$(TARGET)
	gunzip -c $(OBJDIR)/$(TRACE) | $(OBJDIR)/$(TARGET) -t $(T) -l $(L) -a $(A) -opt 0
	@echo
	@echo

VC: $(OBJDIR)/$(TARGET)
	gunzip -c $(OBJDIR)/$(TRACE) | $(OBJDIR)/$(TARGET) -t $(T) -l $(L) -a $(A) -opt 1
	@echo
	@echo