/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 *
 * Autores:
 *    Julian Morua Vindas B54872
 *    Esteban Valverde Hernandez B47214
 *    Santiago Roman Calvo B46180
*/

#ifndef STATISTICS
#define STATISTICS

typedef struct statistics{
  unsigned int references; //holds the total amount of memory references made.
  unsigned int dirty_evictions; //hols the amount of dirty evictions.
  unsigned int load_misses; //holds the amount of misses due to a load instruction.
  unsigned int store_misses; //holds the amount of misses due to a store instruction.
  unsigned int total_misses; //holds the amount of total misses.
  unsigned int load_hits; //holds the amount of hits due to a load instruction.
  unsigned int store_hits; //holds the amount of hits due to a store instruction.
  unsigned int total_hits; //holds the amount of total hits.
} statistics;



/*
* @brief Initializes each of the statistics in 0.
* @return An initialized statistics structure.
*/
statistics initStats(void);

#endif