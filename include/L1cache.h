#ifndef L1CACHE_H
#define L1CACHE_H

#include <netinet/in.h>
#include "statistics.h"
/* 
 * ENUMERATIONS 
 */

/* Return Values */
enum returns_types {
 OK,
 PARAM,
 ERROR
};

/* Represent the cache replacement policy */
enum replacement_policy{
 LRU,
 NRU,
 RRIP,
 RANDOM 
};

/* Represent the cache optimization technique */
enum opt_technique{
 L2,
 VC
};

enum miss_hit_status {
 MISS_LOAD,
 MISS_STORE,
 HIT_LOAD,
 HIT_STORE
};

/*
 * STRUCTS
 */

/* Cache tag array fields */
typedef struct entry {
 bool valid ;
 bool dirty;
 int tag ;
 int rp_value ;
} entry;

/* Cache replacement policy results */
typedef struct operation_result {
 enum miss_hit_status miss_hit;
 bool dirty_eviction;
 int  evicted_address;
 int vict_way;
} operation_result;


/* Cache struct */
typedef struct cache{
  int cachesize_kb;
  int blocksize_bytes;
  int blocks;
  int associativity;
  int lines;
  entry** mem;
} cache;

/* 
 *  Functions
 */

/*
 * Get tag, index and offset length
 * 
 * [in] cache_size: total size of the cache in Kbytes
 * [in] associativity: number of ways of the cache
 * [in] blocksize_bytes: size of each cache block in bytes
 *
 * [out] tag_size: size in bits of the tag field
 * [out] idx_size: size in bits of the index field
 * [out] offset_size: size in bits of the offset size
 */
int field_size_get(int cachesize_kb,
                   int associativity,
                   int blocksize_bytes,
                   int *tag_size,
                   int *idx_size,
                   int *offset_size);

/* 
 * Get tag and index from address
 * 
 * [in] address: memory address
 * [in] tag_size: number of bits of the tag field
 * [in] idx_size: number of bits of the index field
 *
 * [out] idx: cache line idx
 * [out] tag: cache line tag
 */
void address_tag_idx_get(long address,
                         int tag_size,
                         int idx_size,
                         int offset_size,
                         int *idx,
                         int *tag);

/*
* @brief This function intializes a cache struct.
* @param cachesize_kb: is the size of the cache in kilobytes.
* @param blocksize_bytes: is the size of a single cache block in bytes.
* @param associativity: is the number of ways in the cache.
* @return A pointer to a cache struct with all of its elements initialized.
*/
cache* createCache(int cachesize_kb, int blocksize_bytes, int associativity);

/*
* @brief This function frees the allocated memory for a cache structure.
* @param cache1 is a pointer to the cache structure.
*/
void freeCache(cache* cache1);

/*
* @brief This function updates the statistics according the the current result.
* @param stats: is a pointer to the statistics structure that needs to be updated.
* @return operation_result: is a pointer to the operation_result structure that contains the data required to update the statistics.
*/
void updateStats(statistics* stats, operation_result* operation_result);

/*
* @brief This function updates the multi level statistics according the the current result.
* @param stats: is a pointer to the statistics structure that needs to be updated.
* @return operation_result: is a pointer to the operation_result structure that contains the data required to update the statistics.
*/
void updateMultiLevelStats(statistics* L1_stats, operation_result* L1_operation_result, statistics* L2_stats, operation_result* L2_operation_result);

/* @brief Checks if idx, tag and associativity are valid for the memory reference being made.
 * @param idx corresponds to the line index for the memory reference. It must be positive.
 * @param tag corresponds to the block tag for the memory reference. It must be positive.
 * @param associativity is the amount of ways in the cache memorty. It must be a power of 2.
 */
int verify_rp_params(int idx, int tag, int associativity);

/*
 * @brief The algorithm to determine if a memory reference is a hit or miss is always the same
 * regardless of the replacement policy used. This function determines the hit or miss status.
 * In the case of a miss, it also determines if there is any space available in that cache line
 * for the block to be inserted and in which way it can be inserted.

 * @param idx: index field of the block  for the newest memory reference
 * @param tag: tag field of the block for the newest memory reference
 * @param associativity: number of ways of the entry
 * @param loadstore: type of operation, false if load, true if store
 * @param cache_blocks: cache line to read from.
 *
 * @return operation_result: result of the operation
 * @return way: identifier of the way where the hit was found, if not found should remain -1 to indicate a miss.
 * @return space_available: it is set to True if at least one block in the line has a valid = False.
 * @return available_way_id: identifier of the last available way for new data
 * @return hit: it is set to true if the memory reference was a hit, false otherwise.
 */
void determine_hit_status ( int idx,
                            int tag,
                            int associativity,
                            bool loadstore,
                            entry* cache_blocks,
                            operation_result* operation_result,
                            int* way,
                            bool* space_available,
                            int* available_way_id,
                            bool* hit);

/*
 * @brief This function generates tag 2, from tag 1 through a shift of bits.
 * @param l1_tag: tag from L1 cache
 * @param l1_idx_size: index bits of L1 cache
 */
int L1toL2(int l1_tag, int l1_idx_size);

/*
 * @brief This function seraches for an invalidation of a L1 block because a victimization in a L2 block.
 * @param l1_asoc: associativity of L1 cache
 * @param l1_cache_blocks: cache block of L1 to be analyzed
 * @param l2_tag: tag from L2 cache
 * @param diff: difference in the number of bits between tag 1 and tag 2
 */
int invalidateL1(int l1_asoc, entry* l1_cache_blocks, int l2_tag, int diff);

/* 
 * Search for an address in a cache set and
 * replaces blocks using SRRIP(hp) policy
 * 
 * [in] idx: index field of the block
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] loadstore: type of operation false if load true if store
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: return the cache operation return (miss_hit_status)
 * [out] result: result of the operation (returns_types)
 */
int lru_replacement_policy (int idx,
                           int tag,
                           int associativity,
                           bool loadstore,
                           entry* cache_blocks,
                           operation_result* operation_result,
                           bool debug=false);
/* 
 * Invalidates a line in the cache 
 * 
 * [in] tag: tag field of the block
 * [in] associativity: number of ways of the entry
 * [in] debug: if set to one debug information is printed
 *
 * [in/out] cache_block: cache entry to edit
 *
 * return error if tag is not found in cache blocks
 */
int l1_line_invalid_set(int tag,
                        int associativity,
                        entry* cache_blocks,
                        bool debug=false);

/* 
 * Create vc
 * 
 * [in] num_entries: VC associativity
 * [out] cache: pointer to vc created
 */
cache* createVC_FA(int num_entries);      

/*
* @brief This function updates the statistics according the the current result.
* @param L1_stats: is a pointer to the statistics structure that needs to be updated.
* @param VC_stats: is a pointer to the statistics structure that needs to be updated.
* @return L1_operation_result: is a pointer to the operation_result structure that contains the data required to update the statistics.
* @return L2_operation_result: is a pointer to the operation_result structure that contains the data required to update the statistics.
*/
void updateVCStats(statistics* L1_stats, statistics* VC_stats, operation_result* L1_operation_result, operation_result* VC_operation_result);


#endif