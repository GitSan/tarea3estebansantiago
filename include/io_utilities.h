/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 *
 * Autores:
 *    Julian Morua Vindas B54872
 *    Esteban Valverde Hernandez B47214
 *    Santiago Roman Calvo B46180
*/

#ifndef IO_UTILITIES
#define IO_UTILITIES

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include "statistics.h"

typedef struct parameters{
  unsigned int t;     // size of the cache in kilobytes.
  unsigned int l;     // size of the cache line in bytes.
  int a;     // associativity.
  unsigned int opt;   // optimization.
} parameters;

parameters getParams(int argc, char* argv[]);
/*
* @brief Stores the execution parameters in an organized manner. It also converts them to the required data type.
* @param argc is the amount of parameters recieved at the start of the execution.
* @param argv is a string array containing all the parameters.
* @return A parameter struct containing all of the prediction parameters. If a parameter is not specified at execution time it is assigned a 0 by default.
*/

/* L2 parameters */
struct L2params {
	int L2size;
	int L2asoc;
};

/*
 *
 */
L2params get_L2params(	int L1size,
						            int L1asoc);

void printParams(parameters params, L2params L2param);
/*
* @brief Prints in the terminal all the execution parameters in an organized table.
* @param params is a parameter struct with the prediction parameters.
*/

typedef struct traceLine{
  bool LS; // if set to true is a store, if false is a lod
  unsigned int address; // address to operate with
  unsigned int IC; // instruction count from previous call to the present
} traceLine;

bool getTraceLine(traceLine* line);
/*
* @brief This function reads a line from a trace file and stores its information in the provided traceLine structure. It assumes that a file for reading purposes is already open and ready to provide a new line.
* @param line is a pointer to the traceLine structure that will hold the parsed line.
* @return true if a line was properly read; false if there are no more lines to be read.
*/

void printResultsVC(statistics* L1_stats, statistics* L2_stats, double exec_time, parameters params);
/*
* @brief This function prints all the statistics in the terminal for vc.
* @param L1_stats the statistics of L1.
* @param L2_stats the statistics of VC.
* @param exec_time excution time.
* @param params parameter.
*/
void printResultsML(statistics L1_stats, statistics L2_stats, double exec_time, parameters params);
/*
* @brief This function prints all the statistics in the terminal for multilevel.
* @param L1_stats the statistics of L1.
* @param L2_stats the statistics of L2.
* @param exec_time excution time.
* @param params parameter.
*/
#endif
