/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <L2cache.h>

#define NO_INVALIDATION 777

using namespace std;

class L2cache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

	//////////////////////////////
	//							//
	//	TEST NAMES:				//
	//	1.						//
	//	2.						//
	//	3. l1_hit_l2_hit		//	passed
	//	4. l1_miss_l2_hit		//	failed
	//	5. l1_miss_l2_miss		//	passed
	//	6. l1_invalidation_l2	//	
	//							//
	//////////////////////////////

/*
 * TEST1:
 * 1.
 * 2.
 * 3.
 * 4.
 * 5.
 */
//////////////// CODE HERE ////////////////

/*
 * TEST2:
 * 1.
 * 2.
 * 3.
 * 4.
 * 5.
 */
//////////////// CODE HERE ////////////////

/*
 * TEST3: L1 hit L2 hit
 * 1. Random index, tag and associativity
 * 2. Fill a cache entry
 * 3. Force a miss in L1 to fill with desired tag
 * 4. Force a hit in L1
 * 5. Check miss_hit_status for both L1 and L2
 */
TEST_F(L2cache,l1_hit_l2_hit){
	int status = OK;
	struct l1_l2_entry_info l1_l2_info;
	/* Get random arguments for cache entry */
  	l1_l2_info.l1_idx = rand()%64;
  	l1_l2_info.l1_tag = rand()%128;
  	l1_l2_info.l1_associativity = 1 << (rand()%4);
	l1_l2_info.l2_idx = (l1_l2_info.l1_idx) << 1;
	l1_l2_info.l2_tag = (l1_l2_info.l1_tag) >> 1;  //  l1_tag >> diff
	l1_l2_info.l2_associativity = l1_l2_info.l1_associativity*2;
	/* Structs */
  	struct operation_result l1_result = {};
  	struct operation_result l2_result = {};
	DEBUG(debug_on, l1_hit_l2_hit_test);
	enum miss_hit_status expected_L1_hit;
	enum miss_hit_status expected_L2_hit;
  	if (debug_on) {
  	  printf("Entry Info\n l1_idx: %d\n l1_tag: %d\nl1_associativity: %d\nl2_idx: %d\nl2_tag: %d\nl2_associativity: %d\n",
  	    l1_l2_info.l1_idx,
  	    l1_l2_info.l1_tag,
  	    l1_l2_info.l1_associativity,
		l1_l2_info.l2_idx,
		l1_l2_info.l2_tag,
		l1_l2_info.l2_associativity);
  	}
	/* Create cache entries for both levels */
	struct entry L1_cache_line[l1_l2_info.l1_associativity];
	struct entry L2_cache_line[l1_l2_info.l2_associativity];
	/* Fill L1 cache entry */
    for (int i =  0; i < l1_l2_info.l1_associativity; i++) {
      L1_cache_line[i].valid = true;
      L1_cache_line[i].tag = rand()%128;
      L1_cache_line[i].dirty = 0;
      L1_cache_line[i].rp_value = i;
      while (L1_cache_line[i].tag == l1_l2_info.l1_tag) {
        L1_cache_line[i].tag = rand()%128;
      }
    }
	/* Fill L2 cache entry */
    for (int i =  0; i < l1_l2_info.l2_associativity; i++) {
      L2_cache_line[i].valid = true;
      L2_cache_line[i].tag = rand()%64;
      L2_cache_line[i].dirty = 0;
      L2_cache_line[i].rp_value = i;
      while (L2_cache_line[i].tag == l1_l2_info.l2_tag) {
        L2_cache_line[i].tag = rand()%64;
      }
    }
	/* Apply policy, fills L1 and L2 (write-through) */
  	status = lru_replacement_policy_l1_l2(  &l1_l2_info,
											true,	//store
											L1_cache_line,
											L2_cache_line,
											&l1_result,
											&l2_result,
											false,
											1);	// always differ in one bit, since cache's parameters proportion
  	EXPECT_EQ(status, OK);
	/* Apply policy with same arguments to force hit in both levels */
	status = lru_replacement_policy_l1_l2(  &l1_l2_info,
											true,	//store
											L1_cache_line,
											L2_cache_line,
											&l1_result,
											&l2_result,
											false,
											1); // always differ in one bit, since cache's parameters proportion
  	EXPECT_EQ(status, OK);
	expected_L1_hit = HIT_STORE;
	expected_L2_hit = HIT_STORE;
    EXPECT_EQ(l1_result.miss_hit, expected_L1_hit);	// expects a HIT_STORE
	EXPECT_EQ(l2_result.miss_hit, expected_L2_hit);	// expects a HIT_STORE
}// End Test 3

/*
 * TEST4: L1 miss L2 hit
 * 1. Random index, tag and associativity
 * 2. Fill a cache entry
 * 3. Force a miss in L1 with hit in L2
 * 4. Check miss_hit_status for L1 miss and L2 hit
 */
TEST_F(L2cache,l1_miss_l2_hit){
	int status = OK;
	struct l1_l2_entry_info l1_l2_info;
	/* Get random arguments for cache entry */
  	l1_l2_info.l1_idx = rand()%64;
  	l1_l2_info.l1_tag = rand()%128;
  	l1_l2_info.l1_associativity = 1 << (rand()%4);
	l1_l2_info.l2_idx = (l1_l2_info.l1_idx) << 1;
	l1_l2_info.l2_tag = (l1_l2_info.l1_tag) >> 1;
	l1_l2_info.l2_associativity = l1_l2_info.l1_associativity*2;
	/* Structs */
  	struct operation_result l1_result = {};
  	struct operation_result l2_result = {};
	DEBUG(debug_on, l1_hit_l2_hit_test);
	enum miss_hit_status expected_L1_miss;
	enum miss_hit_status expected_L2_hit;
  	if (debug_on) {
  	  printf("Entry Info\n l1_idx: %d\n l1_tag: %d\nl1_associativity: %d\nl2_idx: %d\nl2_tag: %d\nl2_associativity: %d\n",
  	        l1_l2_info.l1_idx,
  	        l1_l2_info.l1_tag,
  	        l1_l2_info.l1_associativity,
			l1_l2_info.l2_idx,
			l1_l2_info.l2_tag,
			l1_l2_info.l2_associativity);
  	}
	/* Create cache entries for both levels */
	struct entry L1_cache_line[l1_l2_info.l1_associativity];
	struct entry L2_cache_line[l1_l2_info.l2_associativity];
	/* Fill L1 cache entry */
    for (int i =  0; i < l1_l2_info.l1_associativity; i++) {
      L1_cache_line[i].valid = true;
      L1_cache_line[i].tag = rand()%128;
      L1_cache_line[i].dirty = 0;
      L1_cache_line[i].rp_value = i;
      while (L1_cache_line[i].tag == l1_l2_info.l1_tag) {
        L1_cache_line[i].tag = rand()%128;
      }
    }
	/* Fill L2 cache entry */
    for (int i =  0; i < l1_l2_info.l2_associativity; i++) {
      L2_cache_line[i].valid = true;
      L2_cache_line[i].tag = rand()%256;
      L2_cache_line[i].dirty = 0;
      L2_cache_line[i].rp_value = i;
	  if (i==0) {
		L2_cache_line[i].tag = l1_l2_info.l2_tag;
	  }
    }
	/* Apply policy, fills L1 and L2 (write-through) */
  	status = lru_replacement_policy_l1_l2(  &l1_l2_info,
											true,	//store
											L1_cache_line,
											L2_cache_line,
											&l1_result,
											&l2_result,
											false,
											1);	// always differ in one bit, since cache's parameters proportion
	EXPECT_EQ(status, OK);
	expected_L1_miss = MISS_STORE;
	expected_L2_hit = HIT_STORE;
    EXPECT_EQ(l1_result.miss_hit, expected_L1_miss);	// expects a HIT_STORE
	EXPECT_EQ(l2_result.miss_hit, expected_L2_hit);		// expects a HIT_STORE
}// End Test 4

/*
 * TEST5: L1 miss L2 miss
 * 1. Random index, tag and associativity
 * 2. Fill a cache entry
 * 3. Force a miss in L1 and L2
 * 4. Check miss_hit_status for both L1 and L2
 */
TEST_F(L2cache,l1_miss_l2_miss){
	int status = OK;
	struct l1_l2_entry_info l1_l2_info;
	/* Get random arguments for cache entry */
  	l1_l2_info.l1_idx = rand()%64;
  	l1_l2_info.l1_tag = rand()%128;
  	l1_l2_info.l1_associativity = 1 << (rand()%4);
	l1_l2_info.l2_idx = (l1_l2_info.l1_idx) << 1;
	l1_l2_info.l2_tag = (l1_l2_info.l1_tag) >> 1;  //  l1_tag >> diff
	l1_l2_info.l2_associativity = l1_l2_info.l1_associativity*2;
	/* Structs */
  	struct operation_result l1_result = {};
  	struct operation_result l2_result = {};
	DEBUG(debug_on, l1_hit_l2_hit_test);
	enum miss_hit_status expected_L1_miss;
	enum miss_hit_status expected_L2_miss;
  	if (debug_on) {
  	  printf("Entry Info\n l1_idx: %d\n l1_tag: %d\nl1_associativity: %d\nl2_idx: %d\nl2_tag: %d\nl2_associativity: %d\n",
  	    l1_l2_info.l1_idx,
  	    l1_l2_info.l1_tag,
  	    l1_l2_info.l1_associativity,
		l1_l2_info.l2_idx,
		l1_l2_info.l2_tag,
		l1_l2_info.l2_associativity);
  	}
	/* Create cache entries for both levels */
	struct entry L1_cache_line[l1_l2_info.l1_associativity];
	struct entry L2_cache_line[l1_l2_info.l2_associativity];
	/* Fill L1 cache entry */
    for (int i =  0; i < l1_l2_info.l1_associativity; i++) {
      L1_cache_line[i].valid = true;
      L1_cache_line[i].tag = rand()%128;
      L1_cache_line[i].dirty = 0;
      L1_cache_line[i].rp_value = i;
      while (L1_cache_line[i].tag == l1_l2_info.l1_tag) {
        L1_cache_line[i].tag = rand()%128;
      }
    }
	/* Fill L2 cache entry */
    for (int i =  0; i < l1_l2_info.l2_associativity; i++) {
      L2_cache_line[i].valid = true;
      L2_cache_line[i].tag = rand()%64;
      L2_cache_line[i].dirty = 0;
      L2_cache_line[i].rp_value = i;
      while (L2_cache_line[i].tag == l1_l2_info.l2_tag) {
        L2_cache_line[i].tag = rand()%64;
      }
    }
	/* Apply policy, miss, fills L1 and L2 (write-through) */
  	status = lru_replacement_policy_l1_l2(  &l1_l2_info,
											true,	//store
											L1_cache_line,
											L2_cache_line,
											&l1_result,
											&l2_result,
											false,
											1); // always differ in one bit, since cache's parameters proportion
  	EXPECT_EQ(status, OK);
	expected_L1_miss = MISS_STORE;
	expected_L2_miss = MISS_STORE;
    EXPECT_EQ(l1_result.miss_hit, expected_L1_miss);	// expects a MISS_STORE
	EXPECT_EQ(l2_result.miss_hit, expected_L2_miss);	// expects a MISS_STORE

}// End Test 5

/*
 * TEST6: L1 invalidation due to a victimization in L2
 * 1. Random index, tag and associativity
 * 2. Fill a cache entry
 * 3. Force a miss, both in L1 and in L2
 * 4. Check for invalidation through function
 */
TEST_F(L2cache,l1_invalidation_l2){
	int status = OK;
	struct l1_l2_entry_info l1_l2_info;
	/* Get random arguments for cache entry */
  	l1_l2_info.l1_idx = rand()%64;
  	l1_l2_info.l1_tag = rand()%128;
  	l1_l2_info.l1_associativity = 1 << (rand()%4);
	l1_l2_info.l2_idx = (l1_l2_info.l1_idx) << 1;
	l1_l2_info.l2_tag = (l1_l2_info.l1_tag) >> 1;  //  l1_tag >> diff
	l1_l2_info.l2_associativity = l1_l2_info.l1_associativity*2;
	/* Structs */
  	struct operation_result l1_result = {};
  	struct operation_result l2_result = {};
	DEBUG(debug_on, l1_hit_l2_hit_test);
	int expected_invalidation;
  	if (debug_on) {
  	  printf("Entry Info\n l1_idx: %d\n l1_tag: %d\nl1_associativity: %d\nl2_idx: %d\nl2_tag: %d\nl2_associativity: %d\n",
  	    l1_l2_info.l1_idx,
  	    l1_l2_info.l1_tag,
  	    l1_l2_info.l1_associativity,
		l1_l2_info.l2_idx,
		l1_l2_info.l2_tag,
		l1_l2_info.l2_associativity);
  	}
	/* Create cache entries for both levels */
	struct entry L1_cache_line[l1_l2_info.l1_associativity];
	struct entry L2_cache_line[l1_l2_info.l2_associativity];
	/* Fill L1 cache entry,
	   one of its entries has the L1 block victimized in L2 */
	int setTag1 = rand()%128;
    for (int i =  0; i < l1_l2_info.l1_associativity; i++) {
      L1_cache_line[i].valid = true;
      L1_cache_line[i].tag = rand()%128;
      L1_cache_line[i].dirty = 0;
      L1_cache_line[i].rp_value = i;
	  if (i==0) {
		L1_cache_line[i].tag = setTag1;
	  }
    }
	/* Truncate L1 tag to match in L2 */
	int setTag2 = setTag1 >> 1;
	/* Fill L2 cache entry */
    for (int i =  0; i < l1_l2_info.l2_associativity; i++) {
      L2_cache_line[i].valid = true;
      L2_cache_line[i].tag = rand()%64;
      L2_cache_line[i].dirty = 0;
      L2_cache_line[i].rp_value = i;
	  if (i==0) {
      	L2_cache_line[i].valid = true;
      	L2_cache_line[i].tag = setTag2;
      	L2_cache_line[i].dirty = 0;
      	L2_cache_line[i].rp_value = i;	//  will have LRU_value = 0, is going to be victimize
	  }
    }
	/* Check through created function if there was an invalidation */
	int victim;
	//find the victim and modify everyone else's rp_value.
    for(int i = 0; i < l1_l2_info.l2_associativity; i++){
      if (L2_cache_line[i].rp_value == 0) { //victim, where zero is LRU_value
        victim = i;
      } else{
        L2_cache_line[i].rp_value -= 1;
      }
    }
	/* Apply created function */
	int l2invl1 = invalidateL1(l1_l2_info.l1_associativity, L1_cache_line, L2_cache_line[victim].tag, 1); // where one is diff
	int result;
	if (l2invl1 == NO_INVALIDATION) {
		result = 0; // there wasn't a invalidation
	}
	else {
		result = 1;	// there was an invalidation
	}
	expected_invalidation = 1;	// set to invalidation
    EXPECT_EQ(result, expected_invalidation);	// expects an invalidation
}// End Test 6