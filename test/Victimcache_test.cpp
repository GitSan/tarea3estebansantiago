/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <gtest/gtest.h>
#include <time.h>
#include <stdlib.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

using namespace std;

class VCcache : public ::testing::Test{
	protected:
		int debug_on;
		virtual void SetUp()
		{
  		/* Parse for debug env variable */
  		get_env_var("TEST_DEBUG", &debug_on);
		};
};

/*
 * TEST1: 
 * Elegir un numero aleatorio para las variables
 * Llenar la linea de cache con un numero distinto al tagx
 * LLenar el VC con con numeros distintos del tagx
 * Consultar en L1 por tagx
 * Revisar miss en ambos caches
 */
TEST_F(VCcache,l1_miss_vc_miss){
	int status = OK;
	bool loadstore =false;
	struct l1_vc_entry_info l1_vc_info;
	int tag=rand()%4096;
	l1_vc_info.l1_tag=rand()%4096;
	l1_vc_info.l1_idx=rand()%1024;
	l1_vc_info.l1_associativity=1 << (rand()%4);
	l1_vc_info.vc_associativity=16;
	// calculo del tag del vc
	int tag_vc=(tag<<10)+l1_vc_info.l1_idx;
	struct entry l1_cache_blocks[l1_vc_info.l1_associativity];
	struct entry vc_cache_blocks[16];
	struct operation_result l1_result = {};
	struct operation_result vc_result = {};
	/* Fill cache line */
    for (int j =  0; j < l1_vc_info.l1_associativity; j++) {

      l1_cache_blocks[j].valid = true;
      l1_cache_blocks[j].tag = rand()%4096;
      l1_cache_blocks[j].dirty = 0;
      l1_cache_blocks[j].rp_value = j;
      while (l1_cache_blocks[j].tag == tag) {
        l1_cache_blocks[j].tag = rand()%4096;
      }
    }

	/* Fill Victim Cache */
	for (int j =  0; j < l1_vc_info.vc_associativity; j++) {

      vc_cache_blocks[j].valid = true;
      vc_cache_blocks[j].tag = rand()%4096;
      vc_cache_blocks[j].dirty = 0;
      vc_cache_blocks[j].rp_value = j;
      while (vc_cache_blocks[j].tag == tag_vc) {
        vc_cache_blocks[j].tag = rand()%4096;
      }
    }

	//Fijar entrada en valor solo disponible en VC
	l1_vc_info.l1_tag=tag;
  	status = lru_replacement_policy_l1_vc(&l1_vc_info,
      	                        	      loadstore,
        	                      	      l1_cache_blocks,
          	                    	      vc_cache_blocks,
            	                  	      &l1_result,
              	                	      &vc_result,false);

  EXPECT_EQ(status, OK);
  //   Check evicted address inserted correctly
  bool correct=false;
	for (int j =  0; j < l1_vc_info.vc_associativity; j++) {
		if(vc_cache_blocks[j].tag==(l1_result.evicted_address<<10)+l1_vc_info.l1_idx){
			correct=true;
		}
    }
  EXPECT_EQ(correct,true);
  EXPECT_EQ(l1_result.miss_hit,MISS_LOAD);
  EXPECT_EQ(vc_result.miss_hit,MISS_LOAD);
}

/*
 * TEST2: 
 * Elegir un numero aleatorio para las variables
 * Llenar la linea de cache con un numero distinto al tagx
 * LLenar el VC con con numeros distintos del tagx e insertar tagx
 * Consultar en L1 por tagx
 * Revisar miss en L1 y hit en VC
 */
TEST_F(VCcache,l1_miss_vc_hit){
	int status = OK;
	bool loadstore =false;
	struct l1_vc_entry_info l1_vc_info;
	int tag=rand()%4096;
	l1_vc_info.l1_tag=rand()%4096;
	l1_vc_info.l1_idx=rand()%1024;
	l1_vc_info.l1_associativity=1 << (rand()%4);
	l1_vc_info.vc_associativity=16;
	// calculo del tag del vc
	int tag_vc=(tag<<10)+l1_vc_info.l1_idx;	
	struct entry l1_cache_blocks[l1_vc_info.l1_associativity];
	struct entry vc_cache_blocks[16];
	struct operation_result l1_result = {};
  	struct operation_result vc_result = {};
	/* Fill cache line */
    for (int j =  0; j < l1_vc_info.l1_associativity; j++) {

      l1_cache_blocks[j].valid = true;
      l1_cache_blocks[j].tag = rand()%4096;
      l1_cache_blocks[j].dirty = 0;
      l1_cache_blocks[j].rp_value = j;
      while (l1_cache_blocks[j].tag == tag) {
        l1_cache_blocks[j].tag = rand()%4096;
      }
    }

	/* Fill Victim Cache */
	for (int j =  0; j < l1_vc_info.vc_associativity; j++) {

      vc_cache_blocks[j].valid = true;
      vc_cache_blocks[j].tag = rand()%4096;
      vc_cache_blocks[j].dirty = 0;
      vc_cache_blocks[j].rp_value = j;
      while (vc_cache_blocks[j].tag == tag_vc) {
        vc_cache_blocks[j].tag = rand()%4096;
      }
		// Asegurar que el victim lo tenga
		if(j==0){
			vc_cache_blocks[j].tag = tag_vc;
		}
    }

	//Fijar entrada en valor solo disponible en VC
	l1_vc_info.l1_tag=tag;
  	status = lru_replacement_policy_l1_vc(&l1_vc_info,
      	                        	      loadstore,
        	                      	      l1_cache_blocks,
          	                    	      vc_cache_blocks,
            	                  	      &l1_result,
              	                	      &vc_result,false);

  EXPECT_EQ(status, OK);
  //   Check evicted address inserted correctly
  bool correct=false;
	for (int j =  0; j < l1_vc_info.vc_associativity; j++) {
		if(vc_cache_blocks[j].tag==(l1_result.evicted_address<<10)+l1_vc_info.l1_idx){
			correct=true;
		}
    }
  EXPECT_EQ(correct,true);
  //   Check evicted address = tag vc
  EXPECT_EQ(vc_result.evicted_address,tag_vc);

  EXPECT_EQ(l1_result.miss_hit,MISS_LOAD);
  EXPECT_EQ(vc_result.miss_hit,HIT_LOAD);
}
