/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include <L1cache.h>
#include <Victimcache.h>

#define DEFAULT_ADDRESS 0
#define ENTRY_VAL 0
#define VC_ASSOC 16
#define KB 1024
#define ADDRSIZE 32
using namespace std;

int vc_operation(const l1_vc_entry_info *l1_vc_info,entry* l1_cache_blocks,bool loadstore,entry* vc_cache_blocks,operation_result* vc_result,operation_result* l1_result){
	bool hit=true;
    int hit_way_id;
    bool space_available;
    int available_way_id;
    determine_hit_status(l1_vc_info->l1_idx,l1_vc_info->l1_tag,VC_ASSOC,loadstore,vc_cache_blocks,vc_result,&hit_way_id,&space_available,&available_way_id,&hit);
	if(hit){
		// Hit
		vc_result->evicted_address=DEFAULT_ADDRESS;
		vc_result->dirty_eviction=false;
		vc_result->miss_hit=HIT_LOAD; //Solo es relevante la información del hit en si		
		
		// Actualizar dirty bit en L1
		l1_cache_blocks[(l1_result->vict_way)].dirty=vc_cache_blocks[hit_way_id].dirty;
		// Actualizar dato en VC
		vc_cache_blocks[hit_way_id].valid=(l1_result->evicted_address!=DEFAULT_ADDRESS)?true:false;
		if(vc_cache_blocks[hit_way_id].valid){
			vc_cache_blocks[hit_way_id].dirty=l1_result->dirty_eviction;
			vc_cache_blocks[hit_way_id].tag=l1_result->evicted_address;
		}
		
		if (space_available){
			if(vc_cache_blocks[hit_way_id].valid){
				for (int i = 0; i < VC_ASSOC; i++){
					if((i!=hit_way_id) && vc_cache_blocks[i].valid){
						(vc_cache_blocks[i].rp_value)++;
					}
					else{
						vc_cache_blocks[i].rp_value=ENTRY_VAL;
					}					
				}
			}
		}
						
	}else{		
		// Miss
		// Revisar si no está lleno
		vc_result->evicted_address=DEFAULT_ADDRESS;
		vc_result->miss_hit=MISS_LOAD; //Solo es relevante la información del miss, no su tipo
		int cont_v=0;
		int disponible=-1;
		for (int i = 0; i < l1_vc_info->vc_associativity; i++){
			if(vc_cache_blocks[i].valid==false){
				cont_v++;
				disponible=i;
			}
		}
		if(cont_v!=0){
			// No esta lleno
			vc_cache_blocks[disponible].tag=l1_result->evicted_address;
			vc_cache_blocks[disponible].valid=(l1_result->evicted_address!=DEFAULT_ADDRESS)?true:false;
			vc_cache_blocks[disponible].rp_value=0;
			if(vc_cache_blocks[disponible].valid){
				for (int i = 0; i < l1_vc_info->vc_associativity; i++){
					if(i!=disponible && vc_cache_blocks[i].valid==true){
						(vc_cache_blocks[i].rp_value)++;
					}
				}
			}

		}else{
			// Victimizacion
			int cont_vict=0;
			while (vc_cache_blocks[cont_vict].rp_value!=VC_ASSOC-1){
				cont_vict++;
			}
			vc_result->evicted_address=vc_cache_blocks[cont_vict].tag;
			vc_result->dirty_eviction=vc_cache_blocks[cont_vict].dirty;
			vc_cache_blocks[cont_vict].tag=l1_result->evicted_address;
			vc_cache_blocks[cont_vict].valid=(l1_result->evicted_address!=DEFAULT_ADDRESS)?true:false;
			vc_cache_blocks[cont_vict].rp_value=0;
			if(vc_cache_blocks[cont_vict].valid){
				for (int i = 0; i < l1_vc_info->vc_associativity; i++){
					if(i!=cont_vict ){
						(vc_cache_blocks[i].rp_value)++;
					}
				}
			}
		}
		


	}
	return OK;	
}



int lru_replacement_policy_l1_vc(const l1_vc_entry_info *l1_vc_info,
      	                      	 bool loadstore,
        	                    	 entry* l1_cache_blocks,
          	                  	 entry* vc_cache_blocks,
            	                	 operation_result* l1_result,
              	              	 operation_result* vc_result,
                	            	 bool debug)
{
	
	int status=lru_replacement_policy (l1_vc_info->l1_idx,l1_vc_info->l1_tag,l1_vc_info->l1_associativity,loadstore,l1_cache_blocks,l1_result,debug);
	if(status==OK){
		if(l1_result->miss_hit==MISS_LOAD || l1_result->miss_hit==MISS_STORE){
			//Logica victim cache
			int vc_status=vc_operation(l1_vc_info,l1_cache_blocks,loadstore,vc_cache_blocks,vc_result,l1_result);			
			if(vc_status!=OK){
				return ERROR;
			}
		}		
   		return OK;
	}
	else{
		return ERROR;
	}		
}
