#include "statistics.h"

statistics initStats(void){
  statistics stats;

  stats.references = 0;
  stats.dirty_evictions = 0;
  stats.load_misses = 0;
  stats.store_misses = 0;
  stats.total_misses = 0;
  stats.load_hits = 0;
  stats.store_hits = 0;
  stats.total_hits = 0;

  return stats;
}
