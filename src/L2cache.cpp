/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include "L1cache.h"
#include "L2cache.h"

/* Defines */
#define DEFAULT_ADDRESS 0
#define KB 1024
#define ADDRSIZE 32
#define NO_INVALIDATION 777
using namespace std;

int lru_replacement_policy_l1_l2(	const l1_l2_entry_info *l1_l2_info,
									bool loadstore,
									entry* l1_cache_blocks,
									entry* l2_cache_blocks,
									operation_result* l1_result,
									operation_result* l2_result,
									bool debug,
									int diff) 
{
  int L1_return_value = verify_rp_params(l1_l2_info->l1_idx, l1_l2_info->l1_tag, l1_l2_info->l1_associativity);
  int L2_return_value = verify_rp_params(l1_l2_info->l2_idx, l1_l2_info->l2_tag, l1_l2_info->l2_associativity);
  /* If return values valid */
  if (L1_return_value ==  OK  && L2_return_value ==  OK) {
    bool L1_hit;
	bool L2_hit;
    int L1_hit_way_id;
	int L2_hit_way_id;
    bool L1_space_available;
	bool L2_space_available;
    int L1_available_way_id;
	int L2_available_way_id;

	/* Dtermine hit status */
    determine_hit_status(l1_l2_info->l1_idx, l1_l2_info->l1_tag, l1_l2_info->l1_associativity, loadstore, l1_cache_blocks, l1_result, &L1_hit_way_id, &L1_space_available, &L1_available_way_id, &L1_hit);
	determine_hit_status(l1_l2_info->l2_idx, l1_l2_info->l2_tag, l1_l2_info->l2_associativity, loadstore, l2_cache_blocks, l2_result, &L2_hit_way_id, &L2_space_available, &L2_available_way_id, &L2_hit);

	/* LRU variables */
    int L1_MRU_value = l1_l2_info->l1_associativity-1;
	int L2_MRU_value = l1_l2_info->l2_associativity-1;
    int LRU_value = 0;
	
	/* Hit in L1 */
    if (L1_hit) {
	  /* Update L1 block */
      //hit block has to be promoted to MRU and every block whose re-reference value was higher must be decreased by 1:
      for(int i = 0; i < l1_l2_info->l1_associativity; i++){
        if(l1_cache_blocks[i].rp_value > l1_cache_blocks[L1_hit_way_id].rp_value){
          l1_cache_blocks[i].rp_value -= 1;
        }
      }  
      l1_cache_blocks[L1_hit_way_id].rp_value = L1_MRU_value;

	  /* Update L2 block */
      for(int i = 0; i < l1_l2_info->l2_associativity; i++){
        if(l2_cache_blocks[i].rp_value > l2_cache_blocks[L2_hit_way_id].rp_value){
          l2_cache_blocks[i].rp_value -= 1;
        }
      }	  
	  l2_cache_blocks[L2_hit_way_id].rp_value = L2_MRU_value;
	  /* Dirty bit analysis */
      if(loadstore){l2_cache_blocks[L2_hit_way_id].dirty = true;} //else stays how it is already.
      //output result is updated (keep in mind the hit_status has been already determined):
      l2_result->dirty_eviction = false;
      l2_result->evicted_address = DEFAULT_ADDRESS;
	}///* Hit in L1 */

	/* Miss in L1 */
	else {

		/* Hit in L2 */
		if (L2_hit) {
	  	  /* Update L2 block */
      	  for(int i = 0; i < l1_l2_info->l2_associativity; i++){
      	    if(l2_cache_blocks[i].rp_value > l2_cache_blocks[L2_hit_way_id].rp_value){
      	      l2_cache_blocks[i].rp_value -= 1;
      	    }
      	  }
		  /* Promote block's rp with MRU value */
	  	  l2_cache_blocks[L2_hit_way_id].rp_value = L2_MRU_value;
          /* Find the victim in L1 and modify everyone else's rp_value */
          int victim;
          for(int i = 0; i < l1_l2_info->l1_associativity; i++){
            if (l1_cache_blocks[i].rp_value == LRU_value) { //victim
              victim = i;
            } else{
              l1_cache_blocks[i].rp_value -= 1;
            }
          }
		  /* The new block from L2 is inserted in L1 */
          l1_cache_blocks[victim].valid = true;//l2_cache_blocks[L2_hit_way_id].valid;
          l1_cache_blocks[victim].tag = (l2_cache_blocks[L2_hit_way_id].tag) << 1;			//// **  fix talked with professor  **////
          l1_cache_blocks[victim].rp_value = L1_MRU_value;
		}///* Hit in L2 */

		/* Miss in L2 */
		else {
		  /* Is there space in L1? */
		  if (L1_space_available){
			//the re-reference value of every valid block must be decreased by 1, and the newly inserted block has MRU_value:
        	for(int i = 0; i < l1_l2_info->l1_associativity; i++){
        	  if (l1_cache_blocks[i].valid) {
        	    l1_cache_blocks[i].rp_value -= 1; //There is no risk of negative rp_value since there is at least one space available.
        	  }
        	}
        	/* The new block is inserted in L1 */
        	l1_cache_blocks[L1_available_way_id].valid = true;
        	l1_cache_blocks[L1_available_way_id].tag = l1_l2_info->l1_tag;
        	l1_cache_blocks[L1_available_way_id].rp_value = L1_MRU_value;

			/* Is there space in L2? */
			/* If there is space in L2 */
			if (L2_space_available) {
			  //the re-reference value of every valid block must be decreased by 1, and the newly inserted block has MRU_value:
        	  for(int i = 0; i < l1_l2_info->l2_associativity; i++){
        	    if (l2_cache_blocks[i].valid) {
        	      l2_cache_blocks[i].rp_value -= 1; //There is no risk of negative rp_value since there is at least one space available.
        	    }
        	  }
        	  /* The new block is inserted in L2 */
        	  l2_cache_blocks[L2_available_way_id].valid = true;
        	  l2_cache_blocks[L2_available_way_id].tag = l1_l2_info->l2_tag;
        	  l2_cache_blocks[L2_available_way_id].rp_value = L2_MRU_value;
			  l2_cache_blocks[L2_available_way_id].dirty = loadstore;
	          //output result is updated (keep in mind the hit_status has been already determined):
	          l2_result->dirty_eviction = false;
	          l2_result->evicted_address = DEFAULT_ADDRESS;
			}///* If there is space in L2 */

			/* If there is no space in L2 */
			else {
		  	  /* Update L2 block */
			  //find the victim and modify everyone else's rp_value.
			  int victim;
        	  for(int i = 0; i < l1_l2_info->l2_associativity; i++){
        	    if (l2_cache_blocks[i].rp_value == LRU_value) { //victim
        	      victim = i;
        	    } else{
        	      l2_cache_blocks[i].rp_value -= 1;
        	    }
        	  }
        	  //determine if the block is dirty and modify the output result accordingly
        	  l2_result->evicted_address = l2_cache_blocks[victim].tag;
        	  if(l2_cache_blocks[victim].dirty){
        	    l2_result->dirty_eviction = true;
        	  } else {
        	    l2_result->dirty_eviction = false;
        	  }
        	  //insert the new block
        	  l2_cache_blocks[victim].valid = true;
        	  l2_cache_blocks[victim].tag = l1_l2_info->l2_tag;
        	  l2_cache_blocks[victim].rp_value = L2_MRU_value;
        	  l2_cache_blocks[victim].dirty = loadstore;

			}///* If there is no space in L2 */

		  }///* Is there space in L1? */

		  /* If there is no space in L1 */
		  else {
			int victim;

			//find the victim and modify everyone else's rp_value.
        	for(int i = 0; i < l1_l2_info->l2_associativity; i++){
        	  if (l2_cache_blocks[i].rp_value == LRU_value) { //victim
        	    victim = i;
        	  } else{
        	    l2_cache_blocks[i].rp_value -= 1;
        	  }
        	}

			int l2invl1 = invalidateL1(l1_l2_info->l1_associativity, l1_cache_blocks, l2_cache_blocks[victim].tag, diff);
			
			/* Since the valid bit is only zero when the cache is initialized,
			 * the invalidation of a block in L1 is done through a readjust of the LRU policy in that level.
			 * Thus, the invalidated block is going to be asigned with de LRU value,
			 * so that in the next iteration, that block is the one that is going to be replaced.
			 */
			if (l2invl1 != NO_INVALIDATION) {
			  for(int i = 0; i < l1_l2_info->l1_associativity; i++){
				if (l1_cache_blocks[i].rp_value == l1_cache_blocks[l2invl1].rp_value) {
					l1_cache_blocks[i].rp_value = LRU_value;
				}
				else if (l1_cache_blocks[i].rp_value > l1_cache_blocks[l2invl1].rp_value) {
					l1_cache_blocks[i].rp_value -= 1;
				}
              }
			  //insert the new block; this time the victimized block will be l2invl1
              l1_cache_blocks[l2invl1].valid = true;
              l1_cache_blocks[l2invl1].tag = l1_l2_info->l1_tag;
              l1_cache_blocks[l2invl1].rp_value = L1_MRU_value;
			}
			else {
			  //find the victim and modify everyone else's rp_value.
              //int victim;
              for(int i = 0; i < l1_l2_info->l1_associativity; i++){
                if (l1_cache_blocks[i].rp_value == LRU_value) { //victim
                  victim = i;
                } else{
                  l1_cache_blocks[i].rp_value -= 1;
                }
              }
              //insert the new block
              l1_cache_blocks[victim].valid = true;
              l1_cache_blocks[victim].tag = l1_l2_info->l1_tag;
              l1_cache_blocks[victim].rp_value = L1_MRU_value;
			}

			/* Same but with L2 */
        	//determine if the block is dirty and modify the output result accordingly
        	l2_result->evicted_address = l2_cache_blocks[victim].tag;
        	if(l2_cache_blocks[victim].dirty){
        	  l2_result->dirty_eviction = true;
        	} else {
        	  l2_result->dirty_eviction = false;
        	}
        	//insert the new block
        	l2_cache_blocks[victim].valid = true;
        	l2_cache_blocks[victim].tag = l1_l2_info->l2_tag;
        	l2_cache_blocks[victim].rp_value = L2_MRU_value;
        	l2_cache_blocks[victim].dirty = loadstore;
      		  
		  }///* If there is no space in L1 */
			
		}///* Miss in L2 */

	}///* Miss in L1 */

  }///* If return values valid */

  if (L1_return_value ==  OK  && L2_return_value ==  OK) {
   return OK;
  }
  else {
	return ERROR; 
  }

}//end of replacement evaluation