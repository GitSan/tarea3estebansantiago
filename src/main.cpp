#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <time.h>
#include <L1cache.h>
#include <L2cache.h>
#include <Victimcache.h>
#include <debug_utilities.h>
#include "io_utilities.h"

#define VC_ASSOC 16
/* Helper funtions */
void print_usage ()
{
  printf ("Print print_usage\n");
  exit (0);
}
/* Main */
int main(int argc, char * argv []) {
  /* Execution time variables */
  clock_t start, end;
  /* Parse argruments */
  parameters params = getParams(argc, argv);
  /* Get L2 params */
  L2params L2param = get_L2params(params.t, params.a);  // L2size = L1size*4; L2asoc = L1asoc*2;
  /* Create cache */
  cache* cL1 = createCache(params.t, params.l, params.a);
  cache* cL2 = createCache(L2param.L2size, params.l, L2param.L2asoc);
  cache* vc_cache=createVC_FA(VC_ASSOC);
  if (cL1 != NULL && cL2 != NULL) {
    /* Determine the field sizes */
    int L1_tag_size, L2_tag_size;
    int L1_idx_size, L2_idx_size;
    int L1_offset_size, L2_offset_size;
    field_size_get(cL1->cachesize_kb, cL1->associativity, cL1->blocksize_bytes, &L1_tag_size, &L1_idx_size, &L1_offset_size);
    field_size_get(cL2->cachesize_kb, cL2->associativity, cL2->blocksize_bytes, &L2_tag_size, &L2_idx_size, &L2_offset_size);
    /* Create the structures required for the simulation */
    statistics L1_stats = initStats();  // initialize L1 statistics
    statistics L2_stats = initStats();  // initialize L2 statistics
   
    statistics L1vc_stats = initStats();  // initialize L2 statistics
    statistics VC_stats = initStats();  // initialize L2 statistics
    
    operation_result L1_output;         // to save the result from L1's memory reference
    operation_result L2_output;         // to save the result from L2's memory reference

    operation_result L1vc_output;         // to save the result from L1's memory reference
    operation_result VC_output;
    /* Trace line variables */
    traceLine tline;      // to read the stdin
    int L1_idx, L2_idx;   // to save the idx for every line read from the trace.
    int L1_tag, L2_tag;   // to save the tag from every line read from the trace.
    /* Difference between tag sizes, L1 vs L2 */
    int diff;
    /* Indicators */
    int check;  // to save the result indicator of a replacement. (Either OK, PARAM or ERROR.)
    /* Get trace's lines and start your simulation */
    start = clock();
    while (getTraceLine(&tline)) {
      address_tag_idx_get(tline.address, L1_tag_size, L1_idx_size, L1_offset_size, &L1_idx, &L1_tag);
      address_tag_idx_get(tline.address, L2_tag_size, L2_idx_size, L2_offset_size, &L2_idx, &L2_tag);
      diff = L1_tag_size - L2_tag_size;
      /* L1_L2 info */
      l1_l2_entry_info l1l2 = {L1_idx, L1_tag, params.a, L2_idx, L2_tag, L2param.L2asoc};
      /* VC_info */
      l1_vc_entry_info l1vc;
      l1vc.l1_idx =L1_idx;
      l1vc.l1_tag =L1_tag;
      l1vc.l1_associativity= params.a;
      l1vc.vc_associativity =VC_ASSOC;
      switch (params.opt) {
        case L2:{
            check = lru_replacement_policy_l1_l2(&l1l2, tline.LS, cL1->mem[l1l2.l1_idx], cL2->mem[l1l2.l2_idx], &L1_output, &L2_output,false,diff);
          break;
        }
        case VC:{
            check =lru_replacement_policy_l1_vc(&l1vc,tline.LS,cL1->mem[l1vc.l1_idx],vc_cache->mem[0],&L1vc_output,&VC_output,false);
          break;
        }
        default: break;
      }
      /* Update stats only if the replacement policy was correctly executed. */
      if (check == OK) {
        if(params.opt == 0){
          updateMultiLevelStats(&L1_stats, &L1_output, &L2_stats, &L2_output);
        }else{
          updateVCStats(&L1vc_stats,&VC_stats,&L1vc_output,&VC_output);
        }
        
      }
    }

    
    end = clock();
    double exec_time = ((double) (end - start)) / CLOCKS_PER_SEC;
    /* Print cache configuration */
    printParams(params, L2param);
    /* Print Statistics */
    if(params.opt == L2){
      printResultsML(L1_stats, L2_stats, exec_time, params);
    }else{
      printResultsVC(&L1vc_stats, &VC_stats, exec_time, params);
    }
    // printResults(L1_stats, L2_stats, exec_time, params);

    /* Free cache */
    freeCache(cL1);
    freeCache(vc_cache);

  }
  else {
    printf("Error al asignar memoria a la  esctructura cache.\n\n");
  }
return 0;
}