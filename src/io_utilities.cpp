#include "io_utilities.h"
#include "L1cache.h"

parameters getParams(int argc, char* argv[]){
  parameters param;
  //Default values are given:
  param.t = 0;
  param.l = 0;
  param.a = 0;
  param.opt = 0;

  for (int i = 1; i < argc; i = i + 2) {
    if (strcmp("-t", argv[i]) == 0) {
      param.t = atoi(argv[i+1]);
    } else if (strcmp("-l", argv[i]) == 0) {
      param.l = atoi(argv[i+1]);
    } else if (strcmp("-a", argv[i]) == 0) {
      param.a = atoi(argv[i+1]);
    } else if (strcmp("-opt", argv[i]) == 0) {
      param.opt = atoi(argv[i+1]);
    }
  }
  return param;
}

L2params get_L2params(int L1size, int L1asoc) {
	L2params L2param;
	L2param.L2size = L1size*4;
	L2param.L2asoc = L1asoc*2;
	return L2param;
}

void printParams(parameters params, L2params L2param){
  const char* division = "------------------------------------------------------------------------";
  printf("%s\n%s\n%s\n",division, "Cache parameters:", division );

  const char* policy;
  switch (params.opt) {
    case 0: {
      policy = "L2";
      printf("L1 Cache Size (KB): \t\t\t\t\t\t %u\n", params.t);
      printf("L2 Cache Size (KB): \t\t\t\t\t\t %u\n", L2param.L2size);
      printf("Cache L1 Associativity: \t\t\t\t\t %u\n", params.a);
      printf("Cache L2 Associativity: \t\t\t\t\t %u\n", L2param.L2asoc);
      printf("Cache Block Size (bytes): \t\t\t\t\t %u\n", params.l);
      printf("%s\n", division);
    break;
    }
    case 1: {
      policy = "VC";
      printf("L1 Cache Size (KB): \t\t\t\t\t\t %u\n", params.t);
      printf("Cache L1 Associativity: \t\t\t\t\t %u\n", params.a);
      printf("Cache Block Size (bytes): \t\t\t\t\t %u\n", params.l);
      printf("%s\n", division);
    break;
    }
    default: {
      policy = "Not Specified";
      printf("Replacement Policy: \t\t\t\t\t %s\n", policy);
      printf("%s\n", division);
    }
  }
}

bool getTraceLine(traceLine* line){
  int bufsize = 32;
  char buffer[bufsize];
  if (fgets(buffer, bufsize, stdin)) {
    line->LS = (bool) atoi(strtok(buffer, "# "));
    line->address = (unsigned int) strtol((strtok(NULL, " ")), NULL, 16);
    line->IC = atoi(strtok(NULL, "\n"));
    return true;
  } else {
    return false;
  }
}

void printResultsVC(statistics* L1_stats, statistics* L2_stats, double exec_time, parameters params){
  
  int vcde=L2_stats->dirty_evictions;
  int vchits=L2_stats->total_hits;
  int thitsvc=L2_stats->total_hits+L1_stats->total_hits;
  int tmvc=L2_stats->total_misses+L1_stats->total_misses;
  double mrvc=(double)((double)thitsvc)/((double)(L1_stats->references+L2_stats->references));
  
  const char* division = "------------------------------------------------------------------------";

      /* Victim Cache */
      printf("%s\n%s\n", "Simulation results:", division);
      printf("Miss rate (L1+VC): \t\t\t\t\t\t %f\n", mrvc);
      printf("Misses (L1+VC): \t\t\t\t\t\t %u\n", tmvc);
      printf("Hits (L1+VC): \t\t\t\t\t\t\t %u\n", thitsvc);
      printf("Victim cache hits: \t\t\t\t\t\t %u\n", vchits);
      printf("Dirty Evictions: \t\t\t\t\t\t %u\n", vcde);
      printf("%s\n", division);
      printf("Execution time: %lf s\n",exec_time);
      printf("%s", division);
}

void printResultsML(statistics L1_stats, statistics L2_stats, double exec_time, parameters params){
  /* L1 rates */
  float L1_read_miss_rate = (float) L1_stats.load_misses/(L1_stats.load_misses + L1_stats.load_hits);
  /* L2 rates */
  float L2_read_miss_rate = (float) L2_stats.load_misses/(L2_stats.load_misses + L2_stats.load_hits);
  /* Global miss rates */
  float gmr = (float) (L2_stats.total_misses)/(L1_stats.references);

  const char* division = "------------------------------------------------------------------------";

      printf("%s\n%s\n", "Simulation results:", division);
      printf("L1 miss rate: \t\t\t\t\t\t\t %f\n", L1_read_miss_rate);
      printf("L2 miss rate: \t\t\t\t\t\t\t %f\n", L2_read_miss_rate);
      printf("Global miss rate: \t\t\t\t\t\t %f\n", gmr);
      printf("Misses (L1): \t\t\t\t\t\t\t %u\n", L1_stats.total_misses);
      printf("Hits (L1): \t\t\t\t\t\t\t %u\n", L1_stats.total_hits);
      printf("Misses (L2): \t\t\t\t\t\t\t %u\n", L2_stats.total_misses);
      printf("Hits (L2): \t\t\t\t\t\t\t %u\n", L2_stats.total_hits);
      printf("Dirty Evictions (L2): \t\t\t\t\t\t %u\n", L2_stats.dirty_evictions);
      printf("%s\n", division);
      printf("Execution time: %lf s\n",exec_time);
      printf("%s", division);
  
}