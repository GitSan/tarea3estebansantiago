/*
 *  Cache simulation project
 *  Class UCR IE-521
 *  Semester: II-2019
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <math.h>
#include <debug_utilities.h>
#include "L1cache.h"
#include "L2cache.h"

/* Defines */
#define DEFAULT_ADDRESS 0
#define KB 1024
#define ADDRSIZE 32
#define NO_INVALIDATION 777
using namespace std;

/* Parameter determinations */
int field_size_get( int cachesize_kb,
                    int associativity,
                    int blocksize_bytes,
                    int *tag_size,
                    int *idx_size,
                    int *offset_size)
{
   *offset_size = log2(blocksize_bytes);
   *idx_size = log2((cachesize_kb * KB)/(blocksize_bytes * associativity));
   *tag_size = ADDRSIZE - *offset_size - *idx_size;
 
   return OK;
}

void address_tag_idx_get(long address,
                        int tag_size,
                        int idx_size,
                        int offset_size,
                        int *idx,
                        int *tag)
{
   address = address >> offset_size;      // offset bits are removed.
   int idx_mask = (1 << idx_size) - 1;    // a mask with 'idx_size' 1s in the least significant bits.
   *idx = address & idx_mask;             // the mask is applied to obtain only the index bits.

   address = address >> idx_size;         // idx bits are removed.
   int tag_mask = (1 << tag_size) - 1;    // a mask with 'tag_size' 1s in the least significant bits.
   *tag = address & tag_mask;             // the mask is applied to obtain inly the tag bits.

}

cache* createVC_FA(int num_entries){
   cache* cache1 = (cache*) malloc(sizeof(cache));
   cache1->lines = 1;
   cache1->mem = (entry**) malloc(sizeof(entry*));
   if (cache1->mem != NULL) {
      cache1->mem[0] = (entry*) malloc(num_entries * sizeof(entry));
         if (cache1->mem[0] != NULL) {
            for (int way = 0; way < num_entries; way++){
               cache1->mem[0][way].valid = false;
               cache1->mem[0][way].dirty = false;
               cache1->mem[0][way].tag = 0;
               cache1->mem[0][way].rp_value = 0;
            }
      }
   }
   return cache1;
}



cache* createCache(int cachesize_kb, int blocksize_bytes, int associativity){
  cache* cache1 = (cache*) malloc(sizeof(cache));
  if (cache1 != NULL) {
    cache1->cachesize_kb = cachesize_kb;
    cache1->blocksize_bytes = blocksize_bytes;
    cache1->blocks = cachesize_kb * KB / blocksize_bytes;
    cache1->associativity = associativity;
    cache1->lines = cache1->blocks/associativity;

    cache1->mem = (entry**) malloc(cache1->lines * sizeof(entry*));
    if (cache1->mem != NULL) {
      for (int line = 0; line < cache1->lines; line++){
        cache1->mem[line] = (entry*) malloc(cache1->associativity * sizeof(entry));
        if (cache1->mem[line] != NULL) {
          for (int way = 0; way < cache1->associativity; way++){
              cache1->mem[line][way].valid = false;
              cache1->mem[line][way].dirty = false;
              cache1->mem[line][way].tag = 0;
              cache1->mem[line][way].rp_value = 0;
          }
        } else {cache1 = 0;}
      }
    } else {cache1 = 0;}
  } else {cache1 = 0;}
  return cache1;
}

void freeCache(cache* cache1){
  for (int line = 0; line < cache1->lines; line++){
    free(cache1->mem[line]);
  }
  free(cache1->mem);
  free(cache1);
}

void updateStats(statistics* stats, operation_result* operation_result){
  stats->references++;
  switch (operation_result->miss_hit) {
    case MISS_LOAD: {stats->load_misses++; stats->total_misses++; break;}
    case MISS_STORE: {stats->store_misses++; stats->total_misses++; break;}
    case HIT_LOAD: {stats->load_hits++; stats->total_hits++; break;}
    case HIT_STORE: {stats->store_hits++; stats->total_hits++; break;}
    default: break;
  }
  if (operation_result->dirty_eviction) {
    stats->dirty_evictions++;
  }
}

void updateMultiLevelStats(statistics* L1_stats, operation_result* L1_operation_result, statistics* L2_stats, operation_result* L2_operation_result){
  L1_stats->references++;
  switch (L1_operation_result->miss_hit) {
    case MISS_LOAD: {L1_stats->load_misses++; L1_stats->total_misses++; break;}
    case MISS_STORE: {L1_stats->store_misses++; L1_stats->total_misses++; break;}
    case HIT_LOAD: {L1_stats->load_hits++; L1_stats->total_hits++; break;}
    case HIT_STORE: {L1_stats->store_hits++; L1_stats->total_hits++; break;}
    default: break;
  }
  L2_stats->references++;
  switch (L2_operation_result->miss_hit) {
    case MISS_LOAD: {L2_stats->load_misses++; L2_stats->total_misses++; break;}
    case MISS_STORE: {L2_stats->store_misses++; L2_stats->total_misses++; break;}
    case HIT_LOAD: {L2_stats->load_hits++; L2_stats->total_hits++; break;}
    case HIT_STORE: {L2_stats->store_hits++; L2_stats->total_hits++; break;}
    default: break;
  }
  if (L2_operation_result->dirty_eviction) {
    L2_stats->dirty_evictions++;
  }
}

void updateVCStats(statistics* L1_stats, statistics* VC_stats, operation_result* L1_operation_result, operation_result* VC_operation_result){
  L1_stats->references++;
  if(L1_operation_result->miss_hit==HIT_LOAD || L1_operation_result->miss_hit==HIT_STORE){
    L1_stats->total_hits++;
  }else{
    L1_stats->total_misses++;
    VC_stats->references++;
    if (VC_operation_result->miss_hit==HIT_LOAD || VC_operation_result->miss_hit==HIT_STORE){
      VC_stats->total_hits++;
    }
    else{
      VC_stats->total_misses++;
    }
    if(VC_operation_result->dirty_eviction){
      VC_stats->dirty_evictions++;
    }  
  }
}

/* Policies related functions */
int verify_rp_params(int idx, int tag, int associativity){
  int result = OK;
  if (idx >= 0 && tag >= 0 && associativity > 0) {
    if (ceil(log2(associativity)) != floor(log2(associativity))){ //whole numbers are the same when rounded-up or rounded-down.
      result = PARAM;
    }
  } else {
    result = PARAM;
  }
  return result;
}

void determine_hit_status (int idx,
                              int tag,
                              int associativity,
                              bool loadstore,
                              entry* cache_blocks,
                              operation_result* operation_result,
                              int* way,
                              bool* space_available,
                              int* available_way_id,
                              bool* hit)
{
  /* Check for hit and save results */
  *hit=false;
  *way=-1;
  int num_available=0;
  *available_way_id=-1;

  for (int i = 0; i < associativity; i++){  // for every block in the line
    if(!(cache_blocks[i].valid)){ // find and empty entry
      num_available++;
      *available_way_id=i;
    }
    if(cache_blocks[i].tag==tag && cache_blocks[i].valid){  // find if the block is already in cache
      *hit=true;
      *way=i;
    }
  }
  /* indicate that there is space available */
  if(num_available>0){
    *space_available=true;
  }else{
    *space_available=false;
  }
   /* Check to update operation result */
   if(loadstore){
    /* Store */
    if (*hit){
      operation_result->miss_hit=HIT_STORE;
    }else{
      operation_result->miss_hit=MISS_STORE;
    }
   }else{
    /* Load */
    if (*hit){
      operation_result->miss_hit=HIT_LOAD;
    }else{
      operation_result->miss_hit=MISS_LOAD;
    }
   }

}

int L1toL2(int l1_tag, /*int l2_tag,*/ int diff) {
  int result = l1_tag >> diff;
  return result;
}

int invalidateL1(int l1_asoc, entry* l1_cache_blocks, int l2_tag, int diff) {
  int result = NO_INVALIDATION;
  int l1tol2;
  for(int i = 0; i < l1_asoc; i++){
    l1tol2 = L1toL2(l1_cache_blocks[i].tag, diff);
    if (l1tol2 == l2_tag) { //victim loses valid property
      result = i;
    }
  }
  return result;
}

int srrip_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
   return ERROR;
}


int lru_replacement_policy (int idx,
                             int tag,
                             int associativity,
                             bool loadstore,
                             entry* cache_blocks,
                             operation_result* result,
                             bool debug)
{
  int return_value = verify_rp_params(idx, tag, associativity);
   if (return_value ==  OK) {
    bool hit;
    int hit_way_id;
    bool space_available;
    int available_way_id;
    determine_hit_status(idx, tag, associativity, loadstore, cache_blocks, result, &hit_way_id, &space_available, &available_way_id, &hit);

    int MRU_value = associativity-1;
    int LRU_value = 0;
    if (hit) {
      //hit block has to be promoted to MRU and every block whose re-reference value was higher must be decreased by 1:
      for(int i = 0; i < associativity; i++){
        if(cache_blocks[i].rp_value > cache_blocks[hit_way_id].rp_value){
          cache_blocks[i].rp_value -= 1;
        }
      }
      cache_blocks[hit_way_id].rp_value = MRU_value;
      if(loadstore){cache_blocks[hit_way_id].dirty = true;} //else stays how it is already.

      //output result is updated (keep in mind the hit_status has been already determined):
      result->dirty_eviction = false;
      result->evicted_address = DEFAULT_ADDRESS;
    } else { //miss
      result->vict_way=-1;
      if (space_available) {
        //the re-reference value of every valid block must be decreased by 1, and the newly inserted block has MRU_value:
        for(int i = 0; i < associativity; i++){
          if (cache_blocks[i].valid) {
            cache_blocks[i].rp_value -= 1; //There is no risk of negative rp_value since there is at least one space available.
          }
        }
        //the new block is inserted:
        cache_blocks[available_way_id].valid = true;
        cache_blocks[available_way_id].tag = tag;
        cache_blocks[available_way_id].rp_value = MRU_value;
        cache_blocks[available_way_id].dirty = loadstore;

        //output result is updated (keep in mind the hit_status has been already determined):
        result->dirty_eviction = false;
        result->evicted_address = DEFAULT_ADDRESS;
      } else { //victimization required:
        //find the victim and modify everyone else's rp_value.
        int victim;
        for(int i = 0; i < associativity; i++){
          if (cache_blocks[i].rp_value == LRU_value) { //victim
            victim = i;
            result->vict_way=victim;
          } else{
            cache_blocks[i].rp_value -= 1;
          }
        }
        //determine if the block is dirty and modify the output result accordingly
        result->evicted_address = cache_blocks[victim].tag;
        if(cache_blocks[victim].dirty){
          result->dirty_eviction = true;
        } else {
          result->dirty_eviction = false;
        }

        //insert the new block
        cache_blocks[victim].valid = true;
        cache_blocks[victim].tag = tag;
        cache_blocks[victim].rp_value = MRU_value;
        cache_blocks[victim].dirty = loadstore;
      } //end of victimization
    }//end of miss
  }//end of replacement evaluation
   return return_value;
}